const fetch = require('node-fetch');
const TelegramBot = require('node-telegram-bot-api');
const token = '';
const bot = new TelegramBot(token, { polling: true });
const mysql      = require('mysql');
const con = mysql.createConnection({
  host     : '',
  user     : '',
  password : '!',
  database : ''
});

const genVoucher = (length) => {
    var text = "";
    var possible =
      "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text
}

const autoEnroll = (userId, courseId) => new Promise((resolve,reject) => {
    
});

const getAllCourse = () => new Promise((resolve,reject) => {
    
});

const getUserByEmail = (email) => new Promise((resolve,reject) => {
  
});

(async () => {
    con.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
    });
    const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/gm;
    let step = 0;
    let step_type;
    let userId;
    let emailUp;
    bot.on('message', async (msg) => {
        const chatId = msg.chat.id;

        if (msg.text.toString() == "/start") {
            bot.sendMessage(chatId, "Selamat data member ETL, input email kalian dengan format emailkalian@gmail.com");
        }

        if (msg.text.toString() == 'command for generate voucher') {
            step = 1;
            step_type = 'genVoucher'
        }

        if (step_type == 'genVoucher' && step == 1) {
            bot.sendMessage(chatId, 'Please input identify password.');
            step = 2;
            step_type = 'genVoucher'
        }

        if (step_type == 'genVoucher' && step == 2) {
            if (msg.text.toString() == 'password generate voucher') {
                const voucher = await genVoucher(5);
                const sql = `INSERT INTO voucher (voucher_code, voucher_is_used) VALUES ('ETL${voucher}', false)`;
                con.query(sql, function (err, result) {
                    if (err) {
                        console.log(err)
                        bot.sendMessage(chatId, `Ada masalah saat generate voucher.`);
                    }
                    bot.sendMessage(chatId, `ETL${voucher}`);
                });


                step = 0;
                step_type = ''
            }
        }

        const email = emailRegex.exec(msg.text.toString());

        if (email) {
            const getUser = await getUserByEmail(email[0]);
            const sql = `SELECT * FROM user WHERE email = '${email[0]}'`;
            con.query(sql, function (err, result) {
                if (err) {
                    console.log(err)
                }

                if (result.length == 0) {
                    const sql1 = `INSERT INTO user (email, voucher_code) VALUES ('${email[0]}', '')`;
                    con.query(sql1, function (err, result) {
                        if (err) {
                            console.log(err)
                        }
                    });
                }
            });
            
            if (getUser.users.length != 0) {
                const user = getUser.users[0];
                bot.sendMessage(chatId, `Halo ${user.name}, kami akan segera aktivasi materimu. Mohon tunggu balasan dari kami untuk proses selanjutnya. \n\nTolong masukan voucher code yang telah diberikan oleh admin. ^_^ Terimakasih banyak sebelumnya.`);
                setTimeout(() => {
                    
                }, 3000);
                step = 1;
                userId = user.id
                emailUp = email[0];
                step_type = 'activation_course'
            }else{
                bot.sendMessage(chatId,`Tidak ada user dengan email ${email[0]} pada website kami, silahkan registrasi terlebih dahulu di https://easytolearn.ancreator.com.`);
                step = 0;
                step_type = ''
            }
        }

        if (step_type == 'activation_course' && step == 1) {
            step = 2;
            step_type = 'activation_course'
        }

        if (step_type == 'activation_course' && step == 2) {
            if (msg.text.toString().includes('ETL')) {
                console.log(msg.text.toString())
                const sql = `SELECT * FROM voucher WHERE voucher_code = '${msg.text.toString()}'`;
                con.query(sql, async function (err, result) {
                    if (err) {
                        console.log(err)
                        step = 0;
                        step_type = ''
                        throw err;
                    }

                    console.log(result)

                    if (result[0].voucher_is_used == 0) {
                        bot.sendMessage(chatId,`Kami akan segera aktivasi course untukmu, mohon tunggu.`);
                        const sql = `UPDATE voucher SET voucher_is_used=true`;
                        con.query(sql, function (err, result) {
                            if (err) {
                                console.log(err)
                                throw err;
                            }
                        });
                        const sql1 = `UPDATE user SET voucher_code='${result[0].voucher_code}' WHERE email='${emailUp}'`;
                        con.query(sql1, function (err, result) {
                            if (err) {
                                console.log(err)
                                throw err;
                            }
                        });
                        const allCourse = await getAllCourse();
                        console.log(allCourse)
                        const courseActive = []
                        await Promise.all(allCourse.courses.map(async data => {
                            const enroll = await autoEnroll(userId, data.id);
                            if (enroll.message == 'User already enrolled in the course.') {
                                courseActive.push({
                                    courseName:data.name,
                                    status:'currently_active'
                                });
                            }

                            if (enroll.is_active) {
                                courseActive.push({
                                    courseName:data.name,
                                    status:'active'
                                });
                            }
                        }));

                        await courseActive.map(data => {
                            if (data.status == 'currently_active') {
                                bot.sendMessage(chatId,`Course : ${data.courseName}, Status: Sudah ter aktivasi sebelumnya.`);
                            } else {
                                bot.sendMessage(chatId,`Course : ${data.courseName}, Status: Berhasil diaktivasi.`);
                            }

                            bot.sendMessage(chatId,`Semua course sudah berhasil di proses, Terimakasih banyak sebelumnya dan selamat belajar.`);
                        })
                        step = 0;
                        step_type = ''
                    }else if(result[0].voucher_is_used == 1){
                        bot.sendMessage(chatId, `Oops, Voucher anda sudah dipakai, Silahkan tanyakan kepada admin.`);
                        step = 0;
                        step_type = ''
                    }else{
                        bot.sendMessage(chatId, `Ada Masalah lain nih :(`);
                        step = 0;
                        step_type = ''
                    }
                });
            }
        }
    });
})()